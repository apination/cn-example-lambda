Apination Connector Template
============================

[aws-console]: https://354661630899.signin.aws.amazon.com/console  "Apination AWS Console"


## Overview

...


## Related Documentation

[sample](https://github.com/Apination/cn-sf#implementation-docs)


## Features

[sample](https://github.com/Apination/cn-sf#features)


## Dependencies

-	[NodeJS ^4.3.2](https://nodejs.org/en/download/)
-	npm dependencies can be automatically installed with `npm install`
	-	A
	-	B
	-	C


## Code Modification

Install the [dependencies](#dependencies) first.

To make sure the code is formatted inline with the project, install [EditorConfig](http://editorconfig.org) and [ESLint](http://editorconfig.org) plugins for your IDE. 

If you are using [Sublime Text 3](https://www.sublimetext.com/3) have the [Package Control](https://packagecontrol.io/installation) installed, the following packages are recommended: EditorConfig, HTML-CSS-JS Prettify, SublimeLinter, SublimeLinter-contrib-eslint (all of them can be installed with `Cmd+Shift+P > Install Package`).

To constantly run all tests on every file modification:

	npm run test-debug


## Deployment

See [apination.github.io](https://apination.github.io/connectors/deployment/package/)
