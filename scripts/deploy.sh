#!/bin/bash
source ./scripts/includes.sh

requires git
requires aws

updateLambdaFunction "cn_example_get_connector_description"
updateLambdaFunction "cn_example_get_updated_transactions"
updateLambdaFunction "cn_example_create_transaction"
