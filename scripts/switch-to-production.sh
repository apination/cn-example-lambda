#!/bin/bash
source ./scripts/includes.sh

requires git
requires aws
requires grep
requires tail
requires sed

if [ "$1" != "-f" ]; then 
	echo ""
	echo "You are about to create new fixed versions of all functions and switch them to production."
	read -p "Would you like to proceed? [y/N] " yn
	echo ""
	if [[ "$yn" != "y" ]]; then exit 1; fi
fi

# turned off to not occasionally make cn_example visible on production
# switchLambdaToProduction "cn_example_get_connector_description"
switchLambdaToProduction "cn_example_get_updated_transactions"
switchLambdaToProduction "cn_example_create_transaction"
