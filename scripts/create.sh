#!/bin/bash
source ./scripts/includes.sh

requires git
requires aws

createLambdaFunction "cn_example_get_connector_description" "index.getConnectorDescription" "nodejs4.3" 
createLambdaFunction "cn_example_get_updated_transactions" "index.getUpdatedTransactions" "nodejs4.3"
createLambdaFunction "cn_example_create_transaction" "index.createTransaction" "nodejs4.3"
