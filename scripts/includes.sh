#!/bin/bash
set -e

APINATION_DEFAULT_REGION=us-east-1
APINATION_DEFAULT_ROLE=arn:aws:iam::354661630899:role/apination_connector_lambda
APINATION_DEFAULT_TIMEOUT=60

# Validates that given app ($1) is installed 
function requires() {
	hash $1 2>/dev/null || { 
		echo >&2 "$1 is required, but it's not installed"
		exit 1
	}
}

# Finds the deployment package path based on current GIT tag
# Usage: PACKAGE=$(findLatestPackage)
function findLatestPackage() {
	local __package=$(ls dist/*.zip | tail -1)
	if [ ! -f $__package ]; then
		echo "" 1>&2
		echo "File $__package could not be found." 1>&2
		echo "Make sure you are runnung the script from the project root folder and you have built the deployment package" 1>&2
		echo "" 1>&2
		exit 1
	fi
	echo "$__package"
}

# Finds the latest GIT tag and writes it to stdout
# Usage: TAG=$(findLatestVersionTag)
function findLatestVersionTag() {
	local __git_tag=$(git describe --abbrev=0 --tags 2>/dev/null)
	local __git_tag=${__git_tag:1}
	echo "$__git_tag" 
}

# Updates lambda function ($1) from a given package ($2) 
# and releases a fixed lambda function version with passed in tag ($3) as description
function createLambdaFunction() {
	local __function_name=${1:?"First parameter must be a function name (e.g. cn_app_do_something)"}
	local __function_handler=${2:?"Second parameter must be a function handler (e.g. index.handler)"}
	local __runtime=${3:?"Third parameter must be AWS Lambda runtime (nodejs4.3 | python2.7)"}
	local __package=$4
	if [ -z "$__package" ]; then local __package=$(findLatestPackage); fi

	echo "Creating $__function_name as $__function_handler from $__package..."
	aws lambda create-function \
		--function-name "$__function_name" \
		--handler "$__function_handler" \
		--zip-file fileb://$__package \
		--runtime $__runtime \
		--region "$APINATION_DEFAULT_REGION" \
		--role "$APINATION_DEFAULT_ROLE" \
		--timeout $APINATION_DEFAULT_TIMEOUT

	echo "Creating STAGE alias for $__function_name..."
	aws lambda create-alias --function-name "$__function_name" --name 'STAGE' --function-version '$LATEST'
}

# Updates lambda function ($1) from a given package ($2) 
# and releases a fixed lambda function version with latest git tag as description
function updateLambdaFunction() {
	local __function_name=${1:?"First parameter must be a function name"}
	local __package=$2
	if [ -z "$__package" ]; then local __package=$(findLatestPackage); fi

	echo "Updating $__function_name code from $__package..."
	aws lambda update-function-code --function-name $__function_name --zip-file fileb://$__package

	echo "Done"
	echo ""
}

function createFixedVersion() {
	local __function_name=${1:?"First parameter must be a function name"}
	local __git_tag=$(findLatestVersionTag)
	aws lambda publish-version --function-name $__function_name --description "$__git_tag" | grep "Version" | sed 's/^.*"\([0-9]\{1,5\}\)".*$/\1/'
}

function getLatestFixedVersion() {
	local __function_name=${1:?"First parameter must be a function name"}
	aws lambda list-versions-by-function --function-name $__function_name | grep "Version" | tail -1 | sed 's/^.*"\([0-9]\{1,5\}\)".*$/\1/'	
}

function updateProdAlias() {
	local __function_name=${1:?"First parameter must be a function name"}
	local __function_version=${2:?"Second parameter must be a function version"}
	aws lambda update-alias --function-name $__function_name --name "PROD" --function-version $__function_version
}

function createProdAlias() {
	local __function_name=${1:?"First parameter must be a function name"}
	local __function_version=${2:?"Second parameter must be a function version"}
	aws lambda create-alias --function-name $__function_name --name "PROD" --function-version $__function_version
}

function switchLambdaToProduction() {
	local __function_name=${1:?"First parameter must be a function name"}
	local __function_version=$(createFixedVersion $__function_name)
	if [ -z "$__function_version" ]; then
		echo >&2 ""
		echo >&2 "$__function_name fixed version could not be created"
		echo >&2 ""
		exit 1
	fi

	echo "Redirecting $__function_name \"PROD\" to \"$__function_version\"..."
	updateProdAlias $__function_name $__function_version || createProdAlias $__function_name $__function_version
} 

