'use strict';

const debug = require('debug')('apination:somethingHappened');
const dateHelper = require('./helpers/dateHelper');
const s3Helper = require('./helpers/s3Helper');

function getDataAsync(dateRanges) {

	return new Promise(function (resolve, reject) {

		// TODO: implement
		const sampleTransactions = require('./sample_data/updates.json');

		resolve(sampleTransactions);
	});
}

/**
 * Sample trigger
 * @param {{apiKey: string, s3:Object, conditions:Object}} event
 * @param {{functionVersion: string, succeed: Function, fail: Function}} context
 * @returns {PromiseLike<{data, stats}>}
 */
module.exports = function getUpdates(event, context) {

	// validate required auth parameters
	if (typeof event.apiKey !== 'string' || !event.apiKey.length) throw new TypeError('event.apiKey argument must be a non-empty String');

	// validate required workflow configuration parameters
	if (!event.conditions) throw new TypeError('event.conditions argument required');
	if (!event.conditions.monitorFromDate) throw new TypeError('event.conditions.monitorFromDate argument required');

	// validate required output parameters
	if (!event.s3) throw new TypeError('event.s3 argument required');
	if (typeof event.s3.bucketName !== 'string' || !event.s3.bucketName.length) throw new TypeError('event.s3.bucketName argument must be a non-empty String');
	if (typeof event.s3.keyPrefix !== 'string' || !event.s3.keyPrefix.length) throw new TypeError('event.s3.keyPrefix argument must be a non-empty String');


	debug('trigger execution started');

	const bucketName = event.s3.bucketName;
	const keyPrefix = event.s3.keyPrefix;
	const previouslySavedMetadata = event.meta;
	const monitorFromDate = event.conditions.monitorFromDate;
	const dateRangesToProcess = dateHelper.getRangesToProcess(monitorFromDate, event.meta);

	debug('retrieving data for the following ranges: %j', dateRangesToProcess);


	return getDataAsync(dateRangesToProcess)
		.then(data => {

			debug('data retrieved, uploading to S3...');

			return s3Helper.uploadDataToS3Async(bucketName, keyPrefix, data);
		})
		.then(uploadResult => {

			const data = uploadResult.data;
			const uploadedTo = uploadResult.uploadedTo;

			const latestUpdateDate = data.length ? data[data.length - 1].LastModifiedDate : null;
			const nextProcessFromDate = dateHelper.getNextProcessFromDate(monitorFromDate, previouslySavedMetadata, latestUpdateDate);

			// TODO: resolve retrieved object reference number(s)
			// https://apination.github.io/connectors/logging/#stats-format
			const transactionsRetrieved = data.length < 10 ?
				data.map(transaction => transaction.invoiceNumber) :
				data.length;

			const executionSummary = {
				// Key within the {bucketName}, where retrieved data is saved.
				// Must be a combination of the input.s3.keyPrefix and an unique GUID
				// e.g. "s3://apination-intermediate-data/2016/07/13/14-57-23-709-6099b022d0944564403c90b5b9d5b1ec4f7b"
				data: uploadedTo,

				// Everything returned in the 'meta' object will be saved for the current trigger_id
				// and submitted back to trigger upon next trigger execution.
				// (See #date-ranges-processing for the usage sample)
				meta: {
					origMonitorFromDate: monitorFromDate,
					processFromDate: nextProcessFromDate
				},

				stats: {
					// A single object name (e.g. contact, transaction, etc.)
					// will be used to format a user-facing log message.
					// For example:
					// - 'Object update retrieved'
					objectName: 'transaction',

					// A plural object name (e.g. contacts, transactions, updates, etc.)
					// will be used to format a user-facing log message.
					// For example:
					// - '25 object updates retrieved'
					// - '3 object updates retrieved (ABC001, ABC002, ABC003)'
					objectNamePlural: 'transactions',

					// A list of user-facing reference numbers or object names.
					// If the number of retrieved objects exceeds reasonable amount,
					// only a total number of retrieved objects must be returned
					// (e.g. 'retrieved: 25')
					retrieved: transactionsRetrieved
				}
			};

			debug('execution complete: %j', executionSummary);

			return executionSummary;
		});
};
