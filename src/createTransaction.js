'use strict';

const debug = require('debug')('apination:doSomething');

function createObjectAsync(apiKey, inputData) {

	return new Promise(function (resolve, reject) {

		// TODO: implement
		const result = {};

		resolve(result);
	});
}

module.exports = function createTransaction(event) {

	// validate required auth parameters
	if (typeof event.apiKey !== 'string' || !event.apiKey.length) throw new TypeError('input.apiKey argument must be a non-empty String');

	// validate required data input
	if (typeof event.post_args !== 'object' || !event.post_args) throw new TypeError('input.post_args argument must be an Object');

	const apiKey = event.apiKey;
	const inputData = event.post_args;

	debug('action execution started');

	return createObjectAsync(apiKey, inputData).then(result => {

		// TODO: resolve created object reference number(s)
		// https://apination.github.io/connectors/logging/#stats-format
		const createdObjectReferenceNumber = 'TEST_NUMBER';

		const executionSummary = {
			stats: {
				// A single object name (e.g. contact, transaction, etc.)
				// will be used to format a user-facing log message.
				// For example:
				// - 'Contact created (ABC001)'
				objectName: 'transaction',

				// A plural object name (e.g. contacts, transactions, updates, etc.)
				// will be used to format a user-facing log message.
				// For example:
				// - '2 contacts updated (ABC002, ABC003)'
				objectNamePlural: 'transactions',

				// List of user-facing reference numbers or object names.
				// If the number of modified objects exceeds reasonable amount,
				// only a total number of retrieved objects must be returned
				// (e.g. 'created: 25')
				created: [createdObjectReferenceNumber],
				updated: 0,
				deleted: 0
			}
		};

		debug('execution complete: %j', executionSummary);

		return executionSummary;
	});
};
