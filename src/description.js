
module.exports = {
	// General connector description.
	// Copy-paste from the specification
	'full_name': 'Example Connector',
	'company_name': 'App Company Name',
	'description': 'App Company Name is ...',

	// Internal name of the connector should start with "cn_",
	// then the name of the connector in lowercase, separated with underscores
	'name': 'cn_example',

	// Configuration of the application page on the Apination website
	'app_page_info': {
		// A list of triggers/actions that will be listed on the application page
		// Copy-paste from the specification
		'events': [
			{ 'name': 'Transaction create/update' }
		]
	},

	// A list of connector triggers
	'triggers': [
		{
			'name': 'get_updated_transactions',

			// A list of top-level parameters expected by the trigger
			'params_for_reg': [

				// A set of parameters needed to connect to 3rd party API.
				// These parameters will be requested on the application connection form.
				{ 'name': 'apiKey', 'description': 'Application API Key', 'required': true },

				// Workflow configuration parameters will be placed within the "conditions" object.
				// These parameters will be requested on the workflow configuration page.
				{ 'name': 'conditions', 'description': 'Parameters object for workflow configuration conditions', 'required': true },

				// A set of parameters required for trigger processing by the HUB. Just leave it here
				{ 'name': 'type', 'description': 'Required parameter for trigger processing, leave it here' },
				{ 'name': 'cron_params', 'description': 'Required parameter for trigger processing, leave it here' },
				{ 'name': 'reverse_watcher_query', 'description': 'Required parameter for trigger processing, leave it here' }
			]
		}
	],

	// A list of connector actions
	'actions': [
		{
			// Action name must describe what the action does in Present Simple
			'name': 'create_transaction',

			// A list od top-level parameters expected by the action (see trigger parameters above)
			'params': [

				// A set of parameters needed to connect to 3rd party API.
				// These parameters will requested on the application connection form
				{ 'name': 'apiKey', 'description': 'Application API Key', 'required': true },

				// A parameter object containing data passed from a workflow
				// e.g. for contact update action it will be a contact data in a format of a destination system
				{ 'name': 'post_args', 'description': 'Contact data' },

				// Required parameter for correct logging. Just leave it here
				{ 'name': 'log_data', 'description': 'Required parameter for action results logging, leave it here' }
			]
		}
	]
}
