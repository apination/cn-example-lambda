'use strict';

const wrapper = require('./helpers/lambdaWrapper');

// Connector description
exports.getConnectorDescription = wrapper.returnJson(require('./description'));

exports.getUpdatedTransactions = wrapper.validateAndExecute(require('./getUpdatedTransactions'));
exports.createTransaction = wrapper.validateAndExecute(require('./createTransaction'));
