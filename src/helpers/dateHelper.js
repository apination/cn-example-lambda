'use strict';

/**
 * Retrieves date ranges that need to be scanned for updates
 * @param  {String} monitorFromDate Monitor From Date value set by user
 * @param  {Object} savedMetadata   Metadata saved by the previous #getFieldsToSave call
 * @return {Array}                  A list of ranges to process, each records is an object with optional from/to values
 */
exports.getRangesToProcess = function (monitorFromDate, savedMetadata) {
	if (isNaN(Date.parse(monitorFromDate))) throw new TypeError('monitorFromDate argument must be a valid date');
	if (savedMetadata && !savedMetadata.origMonitorFromDate) throw new TypeError('savedMetadata, when provided, must contain origMonitorFromDate');
	if (savedMetadata && !savedMetadata.processFromDate) throw new TypeError('savedMetadata, when provided, must contain processFromDate');

	if (!savedMetadata || Date.parse(monitorFromDate) > Date.parse(savedMetadata.processFromDate)) {
		return [{
			from: monitorFromDate,
		}];
	}
	else if (Date.parse(monitorFromDate) < Date.parse(savedMetadata.origMonitorFromDate)) {
		return [{
			from: monitorFromDate,
			to: savedMetadata.origMonitorFromDate
		}, {
			from: savedMetadata.processFromDate
		}];
	}
	else {
		return [{
			from: savedMetadata.processFromDate
		}];
	}
};

/**
 * Determines next processFromDate value
 * @param  {String} monitorFromDate 	Monitor From Date value set by user
 * @param  {Object} savedMetadata   	Metadata saved by the previous #getFieldsToSave call
 * @param  {String} latestUpdateDate	Modification date of the latest updated record retrieved
 * @return {String}                 	processFromDate value to be saved in metadata for the next trigger call
 */
exports.getNextProcessFromDate = function (monitorFromDate, savedMetadata, latestUpdateDate) {
	if (!monitorFromDate) throw new TypeError('monitorFromDate argument required');
	if (latestUpdateDate && typeof latestUpdateDate !== 'string') throw new TypeError('latestUpdateDate argument, when provided, must be a String');

	if (latestUpdateDate) {
		return latestUpdateDate;
	}
	else if (!savedMetadata) {
		return monitorFromDate;
	}
	else if (Date.parse(monitorFromDate) > Date.parse(savedMetadata.processFromDate)) {
		return monitorFromDate;
	}
	else {
		return savedMetadata.processFromDate;
	}
};

/**
 * Formats metadata object to be saved for the next trigger call
 * @param  {String} monitorFromDate 	Monitor From Date value set by user
 * @param  {Object} savedMetadata   	Metadata saved by the previous #getFieldsToSave call
 * @param  {String} latestUpdateDate	Modification date of the latest updated record retrieved
 * @return {Object}                 	Object to be saved as metadata for the next trigger call
 */
exports.getFieldsToSave = function (monitorFromDate, savedMetadata, latestUpdateDate) {
	if (!monitorFromDate) throw new TypeError('monitorFromDate argument required');
	if (latestUpdateDate && typeof latestUpdateDate !== 'string') throw new TypeError('latestUpdateDate argument, when provided, must be a String');

	return {
		origMonitorFromDate: monitorFromDate,
		processFromDate: exports.getNextProcessFromDate(monitorFromDate, savedMetadata, latestUpdateDate)
	};
};
