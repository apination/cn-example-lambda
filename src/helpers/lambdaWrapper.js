'use strict';

/**
 * Validates AWS Lambda parameters passed into main handler.
 * The validation is mainly needed for correct test configuration.
 * @param {Object} input
 * @param {{succeed: Function, fail: Function}} context
 */
function validateAwsLambdaApiParameters(input, context) {
	if (!input)
		throw new TypeError('input argument required');
	if (!context)
		throw new TypeError('context argument required');
	if (typeof context.succeed !== 'function')
		throw new TypeError('context.succeed must be a Function');
	if (typeof context.fail !== 'function')
		throw new TypeError('context.fail must be a Function');
}

/**
 * Validates passed in parameters according to AWS Lambda expected format
 * @param {Function} handler Method to execute in case the passed in parameters are correct
 * @return {Function} Method that performs validation and passes execution further
 */
exports.validateAndExecute = function validateAndExecute(handler) {
	if (typeof handler !== 'function')
		throw new TypeError('handler argument must be a Function');

	return function (input, context) {

		validateAwsLambdaApiParameters(input, context);

		return new Promise((rs, rj) => rs(handler(input, context)))
			.then(context.succeed, context.fail);
	};
};

/**
 * Returns object in Lambda function response
 * @param	{Object}	json	Object to return
 * @return	{Function}
 */
exports.returnJson = function returnJson(json) {
	if (typeof json !== 'object' || !json) throw new TypeError('data argument must be an Object');

	return function (event, context) {
		if (!context) throw new TypeError('context argument required');
		if (typeof context.succeed !== 'function') throw new TypeError('context.succeed must be a Function');

		return context.succeed({
			data: json
		});
	};
};
