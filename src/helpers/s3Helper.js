'use strict';

const aws = require('aws-sdk');
const uuid = require('uuid');

/**
 * @param {String} bucketName
 * @param {String} keyPrefix
 * @param {Object} json
 * @returns {PromiseLike<{ data: Object, uploadedTo:String }>}
 */
exports.uploadDataToS3Async = function (bucketName, keyPrefix, json) {
	if (typeof bucketName !== 'string' || !bucketName.length) throw new TypeError('bucketName argument must be a non-empty String');
	if (typeof keyPrefix !== 'string' || !keyPrefix.length) throw new TypeError('keyPrefix argument must be a non-empty String');
	if (typeof json !== 'object' || !json) throw new TypeError('json argument must be an Object');

	return new Promise(function (resolve, reject) {

		const s3 = new aws.S3();
		const params = {
			Bucket: bucketName,
			Key: keyPrefix + uuid.v4(),
			Body: JSON.stringify(json)
		};

		s3.putObject(params, function (err, data) {
			if (err) {
				reject(err);
			}
			else {
				resolve({
					data: json,
					uploadedTo: `s3://${params.Bucket}/${params.Key}`
				});
			}
		});
	});
};
