'use strict';

const src = require('../../src/');
const context = require('../mocks/context');
const expect = require('chai').expect;

describe('getConnectorDescription method', function() {

	it('exists', () => {
		expect(src).to.respondTo('getConnectorDescription');
	});

	it('returns connector description', () => {

		const r = src.getConnectorDescription({}, context);

		expect(r).to.have.deep.property('data.name').that.is.not.eq('cn_example');
		expect(r).to.have.deep.property('data.full_name').that.is.not.eq('Example Connector');
	});
});
