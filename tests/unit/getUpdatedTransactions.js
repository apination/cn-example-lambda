'use strict';

const src = require('../../src/');
const context = require('../mocks/context');
const expect = require('chai').expect;

describe('getUpdatedTransactions trigger', function () {

	this.slow(1000);
	this.timeout(5000);

	it('exists', () => {
		expect(src).to.respondTo('getUpdatedTransactions');
	});

	it('validates input parameters according to AWS lambda spec', () => {
		expect(() => src.getUpdatedTransactions()).to.throw('input argument required');
		expect(() => src.getUpdatedTransactions({})).to.throw('context argument required');
		expect(() => src.getUpdatedTransactions({}, {})).to.throw('context.succeed must be a Function');
	});

	it('validates connector required parameters', () => {

		const invalidInput = { apiKey: '' };
		return src.getUpdatedTransactions(invalidInput, context).then(ok => {
			throw new Error('must fail');
		}, err => {
			expect(err).to.be.instanceOf(TypeError);
			expect(err).to.have.property('message', 'event.apiKey argument must be a non-empty String');
		});
	});

	// TODO: add your UNIT tests here
});
