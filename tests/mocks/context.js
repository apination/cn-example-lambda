'use strict';

module.exports = {
	functionVersion: '$LATEST',
	succeed: data => data,
	fail: err => {
		throw err;
	}
};
