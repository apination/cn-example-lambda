'use strict';

// TODO: put your AWS credentials into ./credentials.json
// format: { "accessKeyId": "...", "secretAccessKey": "..." }
require('aws-sdk').config.update(require('./credentials.json'));
require('debug').enable('*');

const src = require('../../src/');
const context = require('../mocks/context');
const expect = require('chai').expect;

describe('getUpdatedTransactions trigger', function () {

	this.slow(1000);
	this.timeout(5000);

	it('returns a Promise, that resolves to execution summary', function () {

		const input = {
			apiKey: 'test',
			conditions: {
				monitorFromDate: new Date().toISOString()
			},
			s3: {
				bucketName: 'apination-intermediate-data-stg',
				keyPrefix: '2001/01/01/00-00-00-000-TEST-'
			}
		};

		const executionTask = src.getUpdatedTransactions(input, context);

		expect(executionTask).to.be.a('Promise');

		return executionTask.then(result => {

			expect(result).to.exist;
			expect(result).to.have.property('data').that.is.a('String').that.is.not.empty;

			const outputPrefix = 's3://' + input.s3.bucketName + '/' + input.s3.keyPrefix;
			expect(result.data.substr(0, outputPrefix.length)).to.equal(outputPrefix);

			expect(result).to.have.deep.property('meta.origMonitorFromDate').that.is.a('String').that.is.not.empty;
			expect(result).to.have.deep.property('meta.processFromDate').that.is.a('String').that.is.not.empty;
			expect(result).to.have.deep.property('stats.objectName').that.is.a('String').that.is.not.empty;
			expect(result).to.have.deep.property('stats.objectNamePlural').that.is.a('String').that.is.not.empty;
			expect(result).to.have.deep.property('stats.retrieved').that.is.an('Array').that.has.length(2);
		});
	});
});
